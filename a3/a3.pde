import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.io.FilenameFilter;

import websockets.*;

WebsocketServer socket;

import processing.video.*;

String init_path;
String movie_path;
ArrayList<Movie> movies;
boolean[] play_movie;
int start_movie_idx = 0;
int cur_movie_idx = 0;
float movieEndDuration = 0.12;//029719;//a 'magic number' helpful to find out when a movie finishes playing


Movie myMovie;


// For voice-text conversion
void setup() {
    //noCursor();
    fullScreen();
//size(200, 200);

/*
    // want video frame and opencv proccessing to same size
    cam = new Capture(this, int(width * scale), int(height * scale));
    //cam = new Capture(this, int(width * scale), int(height * scale), "HD Pro Webcam C920", 30);

    opencv = new OpenCV(this, cam.width, cam.height);
    opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);

    cam.start();
*/

    // Initialize set of videos to loop
    init_path = sketchPath() + "/data/";
    movie_path = "videos/";
    movies = new ArrayList<Movie>();
    loadMovies();
    movies.get(cur_movie_idx).play();

/*
myMovie = new Movie(this, init_path + movie_path + "zuckerberg.mp4");
  myMovie.loop();
myMovie.play();
println("DURATION: " + myMovie.duration());
*/

    // Voice-text conversion
    socket = new WebsocketServer(this, 1337, "/p5websocket");
}

void loadMovies() {
    ArrayList<String> movie_names = listFileNames(init_path + movie_path, "mp4");
    for (int i = 0; i < movie_names.size(); i++) {
println("MOVIE: " + init_path + movie_path + movie_names.get(i));
        movies.add(new Movie(this, init_path + movie_path + movie_names.get(i)));
}

    // Start playing from the first movie by default
    play_movie = new boolean[movies.size()];
    for (int i = 0; i < play_movie.length; i++) {
        if (i == start_movie_idx) play_movie[i] = true;
        else play_movie[i] = false;
    }
}

ArrayList<String> listFileNames(String dir, String ext) {
    File file = new File(dir);
    if (file.isDirectory()) {
        ArrayList<String> names = new ArrayList<String>(Arrays.asList(file.list()));
        if (!Objects.equals(ext, "")) {
            for (int i = 0; i < names.size(); i++) {
                String[] exts = names.get(i).split("\\.");
                if (!Objects.equals(exts[exts.length - 1], ext))
                    names.remove(i);
            }
        }
        return names;
    }
    return null;
}

void play_movies() {
    for (int i = 0; i < movies.size(); i++) {
        if (!play_movie[i]) continue;
        movies.get(i).play();
        //image(movies.get(i), 0, 0, width, height);
println("TIME: " + movies.get(i).time() + " | " + "DURATION: " + movies.get(i).duration());
        if (movies.get(i).time() >= movies.get(i).duration()) {
println("END");
            play_movie[i] = false;
            play_movie[(i+1)%movies.size()] = true;
        }
    }
}

void webSocketServerEvent(String msg) {
    println(msg);
}

void draw() {
/*
    if (cam.available() == true) {
        cam.read();

        // load frame into OpenCV 
        opencv.loadImage(cam);

        // it's often useful to mirror image to make interaction easier
        // 1 = mirror image along x
        // 0 = mirror image along y
        // -1 = mirror x and y
        opencv.flip(1);

        faces = opencv.detect();

        // switch to RGB mode before we grab the image to display
        opencv.useColor(RGB);

        // take current state (PImage), useful for debugging
        output = opencv.getSnapshot(); 
    }
*/
/*
    play_movies();
    for (int i = 0; i < movies.size(); i++) {
Movie m = movies.get(i);
        if (play_movie[i]) image(m,0,0,width,height);
    }
*/
background(0);
println("CURRENT MOVIE IDX: " + cur_movie_idx);
image(movies.get(cur_movie_idx),0,0);//,width,height);
//image(myMovie, mouseX, mouseY);
}

// Called every time a new frame is available to read
void movieEvent(Movie m) {
    m.read();
    println(m.time() + " / " + m.duration() + " / " + (m.time() + movieEndDuration));
    //hacky check movie end 
    if((m.time() + movieEndDuration) >= m.duration()){
        println("movie at index " + cur_movie_idx + " finished playback");
        //go to the next movie index
        cur_movie_idx = (cur_movie_idx+1) % movies.size();//increment by one buy use % to loop back to index 0 when the end of the movie array is reached
        //use this to tell the next movie in the list to play
        movies.get(cur_movie_idx).play();
        println("movie at index " + cur_movie_idx + " started");
    }
}
