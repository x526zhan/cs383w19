import websockets.*;

WebsocketClient wsc;

String id = "1"; //str(int(random(10000, 99999)));

void setup() {
  size(400, 400);

  wsc = new WebsocketClient(this, "ws://192.168.1.111:3001");
  //wsc = new WebsocketClient(this, "ws://localhost:3001");
}

void draw() {
  // nothing here
}

void sendMessage(String e) {
    wsc.sendMessage(e);
}

void keyPressed() {
    if (key == BACKSPACE) sendMessage("DELETE");
    else if (key == ENTER) sendMessage("\n");
    else sendMessage(Character.toString(key));
}

void webSocketEvent(String msg) {
  println(msg);
}
