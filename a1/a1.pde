/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Scanner;

// Parameters for drawing & reading
int timer;
color[] colors;
boolean clear_screen = false;
String mem_file_name = "processing.txt";

// Variables
int init_block_width = 44;
int min_block_width = 10;
int init_timestep = 80; // in milliseconds
int min_timestep = 15;
int blank_time = 3000;
int mem_offset_start = 6072;
int mem_offset_end = 20000;
int block_width, timestep, mem_offset;

void setup() {
    noCursor();
    fullScreen();
    timestep = init_timestep;
    mem_offset = mem_offset_start;
    block_width = init_block_width;
}

void draw() {
    /***** WORKING VERSION, REAL-TIME DATA READING ****
    if (millis() - timer >= 200) {
        println(mem_offset + " draw\n");
        int buff_size = (width/block_width) * (height/block_width) * 3 / 2;
        char[] buffer = new char[buff_size];

        BufferedReader reader = createReader(mem_file_name);
        try{
            reader.skip(mem_offset);
            
            int inbuffer = 0;
            while (inbuffer < buff_size) {
                int readsize = reader.read(buffer, inbuffer, buff_size-inbuffer);
                inbuffer += readsize;
            }

            colors = new color[(width/block_width) * (height/block_width)];
            for (int i = 0; i < buff_size/3; i += 1) {
                colors[i*2+0] = color(buffer[i*3+0]%256, buffer[i*3+0]/256, buffer[i*3+1]%256);
                colors[i*2+1] = color(buffer[i*3+1]/256, buffer[i*3+2]%256, buffer[i*3+2]/256);
            }
            for (int i = 0; i < (height/block_width); i ++) {
                for (int j = 0; j < (width/block_width); j ++) {
                    fill(colors[i*(width/block_width)+j]);
                    rect(j*block_width, i*block_width, block_width, block_width);
                }
            }
        } catch (IOException e) {
            println(e);
        }
        //mem_offset += buff_size;
        timer = millis();
    }
    */

    // Assignment version: Read from file
    BufferedReader reader = createReader(mem_file_name);
    if (clear_screen) {
        if (millis() - timer <= blank_time) {
            background(color(0,0,0));
        } else {
            block_width = init_block_width;
            timestep = init_timestep;
            clear_screen = false;
            timer = millis();
        }
    }
    else {
        int w_size = (int)Math.ceil((double)width/block_width);
        int h_size = (int)Math.ceil((double)height/block_width);
        int buff_size = w_size * h_size * 3 / 2;
        char[] buffer = new char[buff_size];
        if (millis() - timer >= timestep) {
            try{
                reader.skip(mem_offset);

                int inbuffer = 0;
                while (inbuffer < buff_size) {
                    int readsize = reader.read(buffer, inbuffer, buff_size-inbuffer);
                    inbuffer += readsize;
                }

                colors = new color[w_size * h_size];

                colorMode(RGB);
                for (int i = 0; i < buff_size/3; i++) {
                    colors[i*2+0] = color(buffer[i*3+0]%256, buffer[i*3+0]/256, buffer[i*3+1]%256);
                    colors[i*2+1] = color(buffer[i*3+1]/256, buffer[i*3+2]%256, buffer[i*3+2]/256);
                }
                for (int i = 0; i < h_size; i++) {
                    for (int j = 0; j < w_size; j++) {
                        fill(colors[i*w_size+j]);
                        rect(j*block_width, i*block_width, block_width, block_width);
                    }
                }
            } catch (IOException e) {
                println(e);
            }
            // This moves the blocks upwards by 1 block at every time step!
            mem_offset += buff_size/h_size;

            // Stop after reading a certain amount of memory addresses & loop
            if (mem_offset > mem_offset_end) {
                mem_offset = mem_offset_start;
                int rand_offset = Math.round(random(1,5));
                block_width -= rand_offset;
                timestep -= 1.5 * rand_offset;
                if (block_width < min_block_width || timestep < min_timestep) {
                    clear_screen = true;
                }
            }

            // ... whereas this moves the blocks entirely
            // mem_offset += buff_size;

            timer = millis();
        }
    }
}
