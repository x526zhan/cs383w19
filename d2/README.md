## D2: Tech Workshop 1 - Generative Output

For D2, I extended the agents in the `agentstarter` code to track colors on an image. Specifically, each agent would draw a color that is closest to its current color, which is implemented by looking up the color of several neighbours and moving in the direction that gives the smallest color difference. The agents draw squares of varying sizes.

### Examples:
![](screenshots/Sonia-Delaunay-demo.jpg)

![](screenshots/autumn-bliss-demo.jpg)

