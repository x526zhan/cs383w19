
class Agent {

    // current position
    float x, y;
    // previous position
    float px, py;

    float x_offset, y_offset;

    // shade
    float shade;

    // color
    color col;

    // create agent that picks starting position itself
    Agent() {
        // default is all agent are at centre
        //x = width / 2;
        //y = height / 2;
        // random starting position
        int m = 0;//100; // margin
        x = random(m, width - m);
        y = random(m, height - m);
        // pick a random grey shade
        shade = 255 * int(random(0, 2));
        col = img.get((int)x, (int)y);
        bezier_step_size = 10;
    }

    // create agent at specific starting position
    Agent(float _x, float _y) {
        x = _x;
        y = _y;
    }

    // Get difference between two colors
    // Reference: https://forum.processing.org/one/topic/function-for-calculating-the-difference-between-two-colors.html
    float colorDist(color c1, color c2) {
        float rmean =(red(c1) + red(c2)) / 2;
        float r = red(c1) - red(c2);
        float g = green(c1) - green(c2);
        float b = blue(c1) - blue(c2);
        return sqrt((int(((512+rmean)*r*r))>>8)+(4*g*g)+(int(((767-rmean)*b*b))>>8));
    }

    void update() {
        // save last position
        px = x;
        py = y;

        if (px <= bezier_step_size) x += bezier_step_size * random(0,maxStepSize);
        if (py <= bezier_step_size) y += bezier_step_size * random(0,maxStepSize);
        if (px >= width-bezier_step_size) x -= bezier_step_size * random(0,maxStepSize);
        if (py >= height-bezier_step_size) y -= bezier_step_size * random(0,maxStepSize);

        float col_diff = Float.MAX_VALUE;
        for (float i = -bezier_step_size; i <= bezier_step_size; i++) {
            for (float j = -bezier_step_size; j <= bezier_step_size; j++) {
                if (i == 0 && j == 0) continue; 
                color c1 = img.get((int)(x + i), (int)(y + j));
                float cur_col_diff = colorDist(col, c1);
                if (cur_col_diff < col_diff) {
                    col_diff = cur_col_diff;
                    x_offset = i;
                    y_offset = j;
                }
            }
        }
        x += random(-maxStepSize,maxStepSize) * x_offset;
        y += random(-maxStepSize,maxStepSize) * y_offset;

        // pick a new position
        //x = x + random(-maxStepSize, maxStepSize);
        //y = y + random(-maxStepSize, maxStepSize);

        // Update color
        col = img.get((int)x, (int)y);

        // Reset offsets
        x_offset = 0;
        y_offset = 0;
    }

    void draw() {

        // draw a line between last position
        // and current position
        strokeWeight(lineWeight);
        //stroke(shade, opacity);
        stroke(col, opacity);
        //line(px, py, x, y);
        //bezier(px, py,px,y,x,py,x,y);
        fill(col, opacity);
        rect(px,py,2*(x-px),2*(y-py));
        /*
        bezier(px,py,
                2/3*px+x/3+random(-maxStepSize,maxStepSize),
                2/3*py+y/3+random(-maxStepSize,maxStepSize),
                px/3+2/3*x+random(-maxStepSize,maxStepSize),
                py/3+2/3*y+random(-maxStepSize,maxStepSize),
                x,y);
        */
    }
}
