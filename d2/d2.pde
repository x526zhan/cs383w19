/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

Gui gui;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

// add your agent parameters here
float maxStepSize = 1;
float flipChance = 0.01;
float opacity = 20;
float lineWeight = 2;
float bezier_step_size = 10;

//String img_name = "starry-night.jpg";
//String img_name = "abstract-action-painting.jpeg";
String img_name = "Sonia-Delaunay.jpg";

PImage img;

void setup() {
  size(800, 600);
  //fullScreen();

  agentsCount = height / 3;
  if (args != null){
        println(args.length);
      println(args[0]);
      img_name = args[0];
} else println("args==null");
  // setup the simple Gui
  gui = new Gui(this);

  gui.addSlider("agentsCount", 10, height);
  gui.addSlider("maxStepSize", 0, 5);
  gui.addSlider("flipChance", 0, 1);
  gui.addSlider("opacity", 0, 255);
  gui.addSlider("lineWeight", 0.5, 10);
  gui.addSlider("bezier_step_size", 5, 20);
  createAgents();
}

void createAgents() {

  background(255);
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  img = loadImage(img_name);
  // resize to width/height
  img.resize(width, height);

  // Draw an almost transparent image
  //tint(255, 25); 
  //image(img,0,0);

  for (int i = 0; i < agentsCount; i++) {
    Agent a = new Agent();
    agents.add(a);
  }

  /*
  for (float x = 100; x < width - 100; x += 5)
    for (float y = 100; y < height - 100; y += 5) {
      Agent a = new Agent(x, y);
      agents.add(a);
    }
  */
}

void draw() {

  // update all agents
  // draw all the agents
  for (Agent a : agents) {
    a.update();
  }

  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }

  // draw Gui last
  gui.draw();

  // interactively adjust agent parameters
  //param = map(mouseX, 0, width, 0, 10);
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  gui.keyPressed();

  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
}

void mouseDragged() {
  Agent a = new Agent(mouseX, mouseY);
  agents.add(a);
}

// call back from Gui
void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}
