### D5

Users compete at typing text.

* BACKSPACE/DELETE - Erase one character at a time
* ENTER - Erases all text and store it in a backlog
* Other text - Normal typing


Note: The work is only set to work on a local network specified by an IP address, and requires multiple keyboards
