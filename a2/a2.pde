/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */

import processing.video.*;

import gab.opencv.*;

// to get Java Rectangle type
import java.awt.*; 

// read files from directory
import java.io.File;
import java.util.Map;
import java.util.Date;
import java.util.Arrays;
import java.util.Objects;
import java.io.FilenameFilter;

Capture cam;
OpenCV opencv;

// Size of montage images
int psize;
int init_psize = 10;
int psize_step = 2;
int psize_min = 2;
int psize_max = 20;
int psize_default = 5;
float face_ratio_min = 0.01f;
float face_ratio_max = 0.50f;
boolean auto_change_size = true;

// Convert width & height to multiples of psize
int ww, hh;

// scale factor to downsample frame for processing 
float scale = 0.5;

// image to display
PImage output;

// array of bounding boxes for face
Rectangle[] faces;

// Path of stored images 
String montaged_img_path = "/data/devin/";
String init_path;

PFont my_font;

int dir_size;
String current_dir;
ArrayList<String> dir_names;
HashMap<String, ArrayList<String> > img_names;

// Store low-res images with averaged rgb values
HashMap<String, int[][]> img_mat_r, img_mat_g, img_mat_b;

// Original & averaged rgb values of a given image
int[][] orig_r, orig_g, orig_b;
int[][] img_r, img_g, img_b;

// Average color of montage images
HashMap<String, int[][]> avg_color;

// Threshold for copies of montaged image
HashMap<String, Integer> montage_threshold;

int new_width, new_height;

void setup() {
    noCursor();
    fullScreen();

    // want video frame and opencv proccessing to same size
    cam = new Capture(this, int(width * scale), int(height * scale));
    //cam = new Capture(this, int(width * scale), int(height * scale), "HD Pro Webcam C920", 30);

    opencv = new OpenCV(this, cam.width, cam.height);
    opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);

    cam.start();

    my_font = createFont("HelveticaNeue", 26);

    // init to empty image
    output = new PImage(cam.width, cam.height);

    new_width = (int)(width * scale);
    new_height = (int)(height * scale);

    init_path = sketchPath() + "/data/";
    dir_names = listDirNames(init_path);
    current_dir = dir_names.get(0);
    dir_size = dir_names.size();

    // Initialize images for Montage
    img_names = new HashMap<String, ArrayList<String> >();
    for (int i = 0; i < dir_size; i++)
        img_names.put(dir_names.get(i), listFileNames(init_path + dir_names.get(i) + "/"));

    orig_r = new int[new_height][new_width];
    orig_g = new int[new_height][new_width];
    orig_b = new int[new_height][new_width];

    img_r = new int[new_height][new_width];
    img_g = new int[new_height][new_width];
    img_b = new int[new_height][new_width];

    img_mat_r = new HashMap<String, int[][]>();
    img_mat_g = new HashMap<String, int[][]>();
    img_mat_b = new HashMap<String, int[][]>();
    avg_color = new HashMap<String, int[][]>();

    montage_threshold = new HashMap<String, Integer>();

    boolean is_updated = set_psize(init_psize, false);
    get_montaged_average_rgb();
}

boolean set_psize(int psize_change, boolean is_change_amt) {
    if (is_change_amt &&
            ((psize == psize_min && psize_change <= 0)||
             (psize == psize_max && psize_change >= 0)))
        return false;
    if (!is_change_amt &&
            (psize_change > psize_max ||
             psize_change < psize_min))
        return false;
    if (is_change_amt)
        psize = Math.max(psize_min, Math.min(psize_max, psize + psize_change));
    else
        psize = Math.max(psize_min, Math.min(psize_max, psize_change));
    ww = (int)(new_width / (float)psize) * psize;
    hh = (int)(new_height / (float)psize) * psize;
    int new_thres = 0;
    for (int i = 0; i < dir_size; i++) {
        String dir = dir_names.get(i);
        int img_count = img_names.get(dir).size();
        new_thres = 50/psize * ww * hh/(psize*psize*img_count);
        montage_threshold.put(dir, new_thres);
    }
    return true;
}

ArrayList<String> listFileNames(String dir) {
    File file = new File(dir);
    if (file.isDirectory()) {
        ArrayList<String> names = new ArrayList<String>(Arrays.asList(file.list()));
        for (int i = 0; i < names.size(); i++) {
            String[] exts = names.get(i).split("\\.");
            if (!Objects.equals(exts[exts.length - 1], "jpg"))
                names.remove(i);
        }
        return names;
    }
    return null;
}

ArrayList<String> listDirNames(String dir) {
    File file = new File(dir);
    FilenameFilter filter = new FilenameFilter() {
        @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
    }; 
    return new ArrayList<String>(Arrays.asList(file.list(filter)));
}

void load_original_rgb(PImage img) {
    color c;
    int h, w;
    int r, g, b = 0;
    img.loadPixels();
    for (int i = 0; i < img.pixels.length; i++) {
        c = img.pixels[i];
        h = i / img.width;
        w = i % img.width;
        r = c >> 16 & 0xFF;
        g = c >> 8 & 0xFF;
        b = c & 0xFF;
        orig_r[h][w] = r;
        orig_g[h][w] = g;
        orig_b[h][w] = b;
    }
}

// Fill psize*psize pads of original image with their respective avg rgb vals
void fill_pad_average_rgb(int x, int y, int h, int w) {
    if (w == 0 || h == 0) return;
    int r = 0;
    int g = 0;
    int b = 0;
    for (int i = x; i < x + h; i++) {
        for (int j = y; j < y + w; j++) {
            r += orig_r[i][j];
            g += orig_g[i][j];
            b += orig_b[i][j];
        }
    }
    r /= w * h;
    g /= w * h;
    b /= w * h;

    for (int i = x; i < x + h; i++) {
        for (int j = y; j < y + w; j++) {
            img_r[i][j] = r;
            img_g[i][j] = g;
            img_b[i][j] = b;
        }
    }
}

void fill_original_average_rgb(PImage img) {
    for (int x = 0; x < hh; x += psize) {
        for (int y = 0; y < ww; y += psize)
            fill_pad_average_rgb(x, y, psize, psize);
        fill_pad_average_rgb(x, ww, psize, img.width - ww);
    }
    for (int y = 0; y < ww; y += psize)
        fill_pad_average_rgb(hh, y, img.height - hh, psize);
    fill_pad_average_rgb(hh, ww, img.height - hh, img.width - ww);
}

void get_montaged_average_rgb() {
    int img_count;
    String dir = "";
    ArrayList<String> img_name;
    for (Map.Entry me : img_names.entrySet()) {
        dir = String.valueOf(me.getKey());
        println("Processing: " + dir);
        img_name = (ArrayList<String>)(me.getValue());
        img_count = img_name.size();
        int[][] montage_r = new int[psize_max][psize_max * img_count];
        int[][] montage_g = new int[psize_max][psize_max * img_count];
        int[][] montage_b = new int[psize_max][psize_max * img_count];
        int[][] montage_color = new int[img_count][4];
        for (int idx = 0; idx < img_count; idx++) {
            PImage img = loadImage(init_path + dir + "/" + String.valueOf(img_name.get(idx)));
            if (img == null) {
                println("ERROR: Not an image!");
                return;
            }
            img.resize(psize_max, psize_max);
            img.loadPixels();

            color c;
            int rr = 0, gg = 0, bb = 0;
            int h, w, r, g, b;
            for (int i = 0; i < img.pixels.length; i++) {
                c = img.pixels[i];
                h = i / psize_max;
                w = i % psize_max;
                r = c >> 16 & 0xFF;
                g = c >> 8 & 0xFF;
                b = c & 0xFF;
                rr += r;
                gg += g;
                bb += b;
                montage_r[h][psize_max * idx + w] = r;
                montage_g[h][psize_max * idx + w] = g;
                montage_b[h][psize_max * idx + w] = b;
            }
            rr /= img.pixels.length;
            gg /= img.pixels.length;
            bb /= img.pixels.length;
            montage_color[idx][0] = rr;
            montage_color[idx][1] = gg;
            montage_color[idx][2] = bb;
            montage_color[idx][3] = 0;
        }
        img_mat_r.put(dir, montage_r);
        img_mat_g.put(dir, montage_g);
        img_mat_b.put(dir, montage_b);
        avg_color.put(dir, montage_color);
    }
}

int get_original_montage_distance(String dir, int x, int y, int z) {
    int[][] val = avg_color.get(dir);
    return (img_r[x][y] - val[z][0]) * (img_r[x][y] - val[z][0]) +
        (img_g[x][y] - val[z][1]) * (img_g[x][y] - val[z][1]) +
        (img_b[x][y] - val[z][2]) * (img_b[x][y] - val[z][2]);
}

PImage fill_original_with_montage(String dir, int img_w, int img_h) {
    int min_dist, dist, min_z;
    int[][] new_r = new int[img_r.length][img_r[0].length];
    int[][] new_g = new int[img_g.length][img_g[0].length];
    int[][] new_b = new int[img_b.length][img_b[0].length];

    int[][] avg_color_temp = avg_color.get(dir);
    int img_count = img_names.get(dir).size();
    float ratio = psize_max / psize;

    for (int x = 0; x < hh; x += psize) {
        for (int y = 0; y < ww; y += psize) {
            min_z = 0;
            min_dist = get_original_montage_distance(dir, x, y, 0);
            for (int z = 1; z < img_count; z++) {
                dist = get_original_montage_distance(dir, x, y, z); 
                if (dist < min_dist && avg_color.get(dir)[z][3] < montage_threshold.get(dir)) {
                    min_dist = dist;
                    min_z = z;
                }
            }

            avg_color_temp[min_z][3]++;
            avg_color.put(dir, avg_color_temp);

            int temp_x, temp_y;
            for (int i = x; i < x + psize; i++) {
                for (int j = y; j < y + psize; j++) {
                    temp_x = int(ratio * (i - x));
                    temp_y = min_z * psize_max + int(ratio * (j - y));
                    new_r[i][j] = img_mat_r.get(dir)[temp_x][temp_y];
                    new_g[i][j] = img_mat_g.get(dir)[temp_x][temp_y];
                    new_b[i][j] = img_mat_b.get(dir)[temp_x][temp_y];
                }
            }
        }
    }

    for (int z = 0; z < img_count; z++)
        avg_color_temp[z][3] = 0;

    PImage new_img = createImage(img_w, img_h, RGB);
    new_img.loadPixels();
    int w, h;
    for (int i = 0; i < new_img.pixels.length; i++) {
        h = i / new_img.width;
        w = i % new_img.width;
        new_img.pixels[i] = color(new_r[h][w], new_g[h][w], new_b[h][w]);
    }
    new_img.updatePixels();
    return new_img;
}

String key_to_dir(char key) {
    ArrayList<String> names = new ArrayList<String>();
    for (int i = 0; i < dir_names.size(); i++) {
        String temp = dir_names.get(i);
        if (temp.toLowerCase().startsWith(Character.toString(key)))
            names.add(temp);
    }
    if (names.size() == 0) return null;
    return names.get((int)random(names.size()));
}

void draw() {
    if (cam.available() == true) {
        cam.read();

        // load frame into OpenCV 
        opencv.loadImage(cam);

        // it's often useful to mirror image to make interaction easier
        // 1 = mirror image along x
        // 0 = mirror image along y
        // -1 = mirror x and y
        opencv.flip(1);

        faces = opencv.detect();

        // switch to RGB mode before we grab the image to display
        opencv.useColor(RGB);

        // take current state (PImage), useful for debugging
        output = opencv.getSnapshot(); 
    }

    // draw the image
    pushMatrix();
    scale(1 / scale); // inverse of the downsample scale
    load_original_rgb(output);
    fill_original_average_rgb(output);
    PImage new_img = fill_original_with_montage(current_dir, output.width, output.height);
    image(new_img, 0, 0);
    popMatrix();

    if (faces != null) {
        int max_w = 0;
        int max_h = 0;
        for (int i = 0; i < faces.length; i++) {
            // scale the tracked faces to canvas size
            float s = 1 / scale;
            int w = int(faces[i].width * s);
            int h = int(faces[i].height * s);
            if (w > max_w) max_w = w;
            if (h > max_h) max_h = h;
        }
        if (auto_change_size) {
            float face_ratio = (float)(max_w * max_h / float(width * height));
            float new_psize = psize_min + (face_ratio-face_ratio_min)/
                (face_ratio_max-face_ratio_min)*(psize_max-psize_min);
            set_psize((int)new_psize, false);
        }
    }

    // nfc is a function to format numbers, second argument is 
    // number of decimal places to show
    //text(nfc(frameRate, 1), 20, 20);
    textFont(my_font);
    text("@" + current_dir, width - 230, height - 30);
}

void keyPressed() {
    String dir = key_to_dir(key);
    if (dir != null) current_dir = dir;
    if (key == ' ') {
        auto_change_size = !auto_change_size;
        if (!auto_change_size) set_psize(psize_default, false);
    }
    int val = Character.getNumericValue(key);
    if (val > 0 && val <= 9) current_dir = dir_names.get(val - 1);
    if (key == CODED) {
        boolean is_updated = false;
        if (keyCode == UP || keyCode == RIGHT) is_updated = set_psize(2, true);
        else if (keyCode == DOWN || keyCode == LEFT) is_updated = set_psize(-2, true);
    }
}
