import websockets.*;
import java.util.concurrent.ConcurrentLinkedQueue;

WebsocketServer ws;

String typing = "";
String saved = "";
String m_string = "";
boolean start = false;
PFont f;

void setup() {
  size(400, 400);
  ws = new WebsocketServer(this, 3001, "");
  f = createFont("Arial", 16);
  background(0);
}

void draw() {
    background(0);

    while (!q.isEmpty()) {
        Message m = q.poll();
        start = true;
        m_string = m.e;
    }
    if (start) {
        if (m_string.equals("DELETE")) {
            typing = typing.substring(0, max(0, typing.length()-1));
        }
        else if (m_string.equals("\n")) {
            // ENTER clears the current content and saves it
            saved = saved + typing;
            typing = "";
        }
        else {
            typing = typing + m_string;
            println("typing: " + typing);
        }
    }
    background(0);
    textSize(32);
    fill(255,255,255);
    text(typing, 10, 10, width-10, height-10);
    start =false;
}

// data structure to hold and parse each incoming socket message
class Message {

    Message(String msg) {
        e = msg;
    }

    String toString() {
        return e;
    }

    String e;
}

void keyPressed() {
    ws.sendMessage("Hi from server.");
}

// special threadsafe queue to transfer input messages 
// from socket thread to main thread
ConcurrentLinkedQueue<Message> q = new ConcurrentLinkedQueue<Message>();
// callback when client sends message
// NOTE: this isn't on the drawing thread, so you can't draw here
void webSocketServerEvent(String msg) {
    Message m = new Message(msg);
    println(m);

    // drawing here may not work because of thread boundaries
    //noStroke();
    //fill(255);
    //ellipse(m.x, m.y, 10, 10);
    // add this message to the threadsafe queue
    q.offer(m);
}
